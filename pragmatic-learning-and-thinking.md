# Pragmatic Learning and Thinking


## Introduction

Skills required apart from coding (Not limited to):

1. Communicating Skills
2. Learning and Thinking skill
3. Finance
4. Management 
5. Negotiations
6. Market insight
7. Much more


> It's not the Teacher teaches. It is the Learner learns


**Consider the Context**

Everything is interconnected. So this means, small things have large effects.

Example: [GTA JSON Story: How LL analysis of disambelled code lead to insight?](https://twitter.com/gwenshap/status/1366516177971814400)



## Book Distilled


### Journey from Novice to Expert

> Dreyfus Model of skill acquisition

1. Harnessing and applying your own *experience*
2. Undestanding *context*
3. Harnessing *intuition*

---

- Your MIND distilled
- Get into your MIND
- Debug you MIND



### Learning Deliberately


Learning techniques

1. Planning techniques
2. Mind maps
3. Reading technique such as **SQ3R**
4. Cognitive importnce of **teaching**and **writing**

---

- Gain EXPERIENCE
- Manage FOCUS
- Beyond expertise
  

## Chapter 2: Journey from NOVICE to EXPERT

### Primer: Event theory vs Construct theory

1. **Event Theory**:
  - Can be measured
  - Can be VERIFIED and PROVEN
  - Can judge the accuracy of EVENT THEORY

2. **Construct Theory**:
  - Are intangible abstractions
  - Makes no sense in *proving them*
  - Evaluated in terms of usefulness
  - **Dreyfus Thoery is a Construct theory**


### Novice vs Expert


*Novices* are the ones who do their thing by being told exactly what to do. Following programming syntax or proprtions of saffron for cooking.

*Expert* are the ones who live with intuition. They have insights on a programming language or know what amount of safforn is just right without needing to know the exact proportion.

### 5 Dreyfus Model Stages

#### 👨‍🎨 Stage 1: Novices

> Novices need context-free recipes/rules


These people have not enough experience to know which rules to use from the ruleset.

#### 👨‍🍳 Stage 2: Advanced Beginner

> Advanced beginner do not need the bigger picture

Thee people do not want to know details. They want to get things done. Not knowing enough details can be an anti-pattern. Go thing about this stage is that it is based on practice and final product and less on theory. 

Recommendation: Know enoguh details to get things done.

#### 👨‍💼 Stage 3: Competent

> Competent can troubleshoot

These people develop conceptual models of the domain in hand. They can figure out solutions to problems they have not faced before.
They seek advice from experts and use if effectively. They are problem solvers in times of uncertainity.

#### 👨‍🚀 Stage 4: Profecient

> Proficient practiitoners can self-correct


These people need the bigger picture. They will seek out and want to understand the larger conceptual framework around this skill. They will be very frustrated by oversimplified information.

They can learn from the experience of others. As a proficient practitioner, you can read case studies, listen to water-cooler gossip of failed projects, see what others have done, and learn effectively from the story, even though you didn’t participate in it firsthand.


For instance, a well-known maxim from the extreme programming methodology tells you to “test everything that can possibly break.”

To the novice, this is a recipe. What do I test? All the setter and getter methods? Simple print statements? They’ll end up testing irrelevant things.

But the proficient practitioner knows what can possibly break—or more correctly, what is likely to break. They have the experience and the judgment to understand what this maxim means in context. And context, as it turns out, is key to becoming an expert.

Proficient practitioners have enough experience that they know—from experience—what’s likely to happen next; and when it doesn’t work out that way, they know what needs to change. It becomes apparent to them which plans need to be discarded and what needs to be done instead.


#### 👨‍💻 Stage 5: Expert 

> Experts need Context-dependant Intuition

The expert knows the difference between irrelevant details and the very important details, perhaps not on a conscious level, but the expert knows which details to focus on and which details can be safely ignored. The expert is very good at targeted, focused pattern matching.



### Herding Racehorces (Experts) and Racing Sheeps (Novice)

In an Agile team of Novices and Advanced Beginners, the Agile project will fail.

Experts cannot train Novices using expert's rules, since the empathy between expert and novice is infinite. 

> Use intuition for experts and rules for novices.

The 3 transformations required for moving from Novice to Expert

1. Moving away from *rules* to *intuition*
2. Change in perception, where a problem is no longer a collection of equally relevant bits but a complete and unique whole where only certain bits are relevant.
3. Change from being a detached observer of the problem to an involved part of the system itself

![Dreyfus Pyramid](https://moleseyhill.com/images/dreyfus-model.png)


### Sad reality of skill distribution in real world

It might feel that skill distibution would be a bell curve, but the reality is that it is skewed towards *Advanced Beginner* and *Novice*.
Most people do copy-pasting and bad application of design patterns.

![Skill distribution](https://uploads-ssl.webflow.com/5ad143610f7efd77b6f188f3/5b2f897d3ad7a62d44909675_pasted-image-0-1%20(2).png)

> Experts are not good teachers

It takes 10 years to become an expert. Good news is that it is easier to become expert in other areas once you have become an expert in one area.


### Using the Dreyfus Model effectively

Nursing industry in 1970s were thought of a mere commodity who were disposable. This lead to bad nursing practices. The only solution was to make nurses believe in an outcome, PATIENT wellbeing.

Let's put this context in coder's POV:

1. Coders are a mere-commodity. These people will not have input on the project's design and architecture. 
2. Coders become managers because of lucrative money, leaving the incentive to become experts in the grave.
3. Software engineering education based on formal methods faltered real-world experience. Over-reliance on tools and formal methods eroded real-world experience
4. Lost sight of outcome -- Project outcomes/shipping to customers.

We are mostly Advanced beginners (as seen by [Skill distribution](###Sad reality of skill distribution in real world) )
Since we see these people follow orders without understanding the context, these people aren’t capable of making these sorts of decision by themselves. This is a major anti-pattern. Hence, advanced beginners need to raise their skill levels to competent and above.

> Learn by watching and imitating.

The 3 steps to do to learn:
1. Imitate
2. Assimialte
3. Innovate

These is not substitute for expertise without experience. 

Providing a right environment for skilled developers is critical. Don't spoon-feed them. Make them to think independantly.

> Keep practicing to remain an expert. (Tech lead should also see code)

### Beware the Tool Trap

> Model is a tool, not a mirror. There is not silver bullet for a model/tool. Capability Maturity Model (CMM) and UML are not silver bullets.

**Why formal methods are counter-productive and avoid them if you need creativity, intuition, or inventiveness.**

1. Devaluing traits that cannot be formalized

Higher management think they can put creativity and invention on a time clock. This is not true since ideas for formed though thorough understanding and a bigger picture.

2. Legislating behaviour that do not respect Individual Autonomy

Over-relaince on formal methods lead to lack of individual behaviour. This leads to herd behaviour which is a major red-flag.

3. Alienating experience practioners in favour to Novices

4. Spelling out too much detail is overwhelming

5. Oversimplification of complex situations

In software projects, follwing hard rules such as DO THESE THINGS... is destructuve instead of constructive

6. Lack of nuance performance

Formal methods remove nuances which is a red flag.


### Consider the context, again

> Context matters, but the lower several stages on the Dreyfus model aren’t skilled enough to know it. So once again, we have to look at ways of climbing the Dreyfus ladder.

### Day-to-day Dreyfus

We need novices and experts  in a team. Novices can bring some stupd detail that can be actually helpful. Novices do not have biases, unlike experts. 

> Learn the skill of learning.




## Chapter 3: This is your BRAIN

## Chapter 4: Get into your MIND

## Chapter 5: Debug your MIND


## Chapter 6: Learn deliberately

## Chapter 7: Gain Experience


## Chapter 8: Manage FOCUS    

## Chapter 9:Beyond expertie

> Professional KISS of death is when the expert thinks he is an expert.


### Effective change

End of book, new habits will be hard to create. Mind will treat information to cache rather than store to disk. New information needs to have an emotional weightage to be stored to your disk part of your brain.

Tips for effective change:

1. Start with a plan

- Block some time and keep it consistent.
- Keep track of accomplishments
- Review accomplishments when you feel you have not done enough.
- Use wiki, webapp or journal.

2. Inaction is the enemy not the error.

3. Belief is REAL

> If you think you are going to fail, you will. Thoughts physically alter your brain and your brain chemistry; hence control your thoughts diligently.


4. Take small steps and finish them

Take small steps and finish. Do not put tasks/learnings in IN-PROGRESS.

[Use this framework to learn.](https://www.youtube.com/watch?v=FKTxC9pl-WM&list=PL055Epbe6d5ZqIHE7NA5f6Iq_bZNjuWvS)

### DO IT NOW


1. Practice: Having trouble with piece of code? Write it 5 different ways.
2. Plan on making mistakes. Mistakes are good. Learn from them.
3. Keep notes. Keep your though free and flowing.
4. Find a soothing place. Be it music, desk or whatever.
5. Start a wiki. Wiki == Structure notes.
6. Be on social media: Start blogging. Comment on books you have read. 
7. Use SQ3R and mind maps
8. Use virtualized environments to experiment things.



