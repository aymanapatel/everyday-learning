import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

/*

      <!-- Benchmarking dependencies -->
        <dependency>
            <groupId>org.openjdk.jmh</groupId>
            <artifactId>jmh-core</artifactId>
            <version>1.19</version>
        </dependency>
        <dependency>
            <groupId>org.openjdk.jmh</groupId>
            <artifactId>jmh-generator-annprocess</artifactId>
            <version>1.19</version>
        </dependency>


*/

// Source: http://www.rationaljava.com/2015/02/jmh-how-to-setup-and-run-jmh-benchmark.html

/** Can run using package jar. This way is RECOMMENDED
 * java - jar benchmark.jar -h
 * java -jar benchmark.jar -lprof - run with the benchmarks with a profiler. 
 *  To list the available profilers on your system (for example perf is only available on Unix) use the command.
 * java -jar benchmark.jar -prof stack - simple stack profiler (available on all systems) 
 **/
public class BenchmarkRunMain {

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                            .include(BenchmarkClass.class.getSimpleName()) // BenchmarkClass has @Setup and @Teardown similar to Junit
                            .forks(1)
                            .build();

        new Runner(opt).run();
    }
}
