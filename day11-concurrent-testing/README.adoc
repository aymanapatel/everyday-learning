= Concurrent testing
:toc:

== Non-Junit Runner libraries


=== Remember

https://stackoverflow.com/questions/48752447/how-to-run-spring-unit-tests-with-a-different-junit-runner[Running with a different Junit runner requires `@ClassRule` and `@Rule` ]

```java
    @ClassRule
    public static final SpringClassRule springClassRule = new SpringClassRule();
    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();
```


1. mycila

```
import com.mycila.junit.concurrent.Concurrency;
import com.mycila.junit.concurrent.ConcurrentJunitRunner;
```


2. vmlens.ConcurrentTestRunner 


```
import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;
```


== Benchmarking Java 8 
