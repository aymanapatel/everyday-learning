
import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

// TODO: Remove this after testing
@RunWith(ConcurrentTestRunner.class) // By default create 4 methods
@ContextConfiguration(...)
public class MyTest {

    // https://stackoverflow.com/questions/48752447/how-to-run-spring-unit-tests-with-a-different-junit-runner

    @ClassRule
    public static final SpringClassRule springClassRule = new SpringClassRule();
    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private CustomService customService;

    @Test
    public void testCustomService_success() {
        System.out.println("OhYeah");
        // test logic
        
    }

}
