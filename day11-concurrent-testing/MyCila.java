import com.mycila.junit.concurrent.Concurrency;
import com.mycila.junit.concurrent.ConcurrentJunitRunner;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@RunWith(ConcurrentJunitRunner.class)
@Concurrency(8)
@ContextConfiguration(...)
public class MyCila {


    @ClassRule
    public static final SpringClassRule springClassRule = new SpringClassRule();
    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private ProcessInstanceListService processInstanceListService;

    // source
    // https://stackoverflow.com/a/7940706/9642851
    @Test
    public final void runConcurrently() throws InterruptedException {

        ExecutorService exec = Executors.newFixedThreadPool(4);

        for (int i = 0; i < 100; i++) {
            exec.execute(() -> testProcessInstanceList_success());
        }
        exec.shutdown();
        exec.awaitTermination(50, TimeUnit.SECONDS);
    }


    public void test_custom() {
        System.out.println("OhYeah");
        // test logic
    }
