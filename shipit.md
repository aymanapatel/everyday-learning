# Ship It notes



>  Extraordinary products are merely side effects of good habits.

What are these *habits*?

- Don't do what you see around you. Look beyond work to get insights.

- Steps to incorporate good habits (UML)
  1. Find a development methodology to research
  2. Extract one habit that looks good to you (and that can be used by itself).
  3. Put it to use for a week.
  4. If you like it and it seems beneficial
  5. Continue using it for a month.
  6. Practice the new habit until it becomes a natural part of your routine
  7. Start from 1. again for new metodology.
 
 - Be vigilant on removing things that do not work.
 
 
 
 
 ## Appendix
 
 
 
 ### Testing tools
 
 #### Test harness
 
 - SUnit (Smalltalk implementation of xUnit)
 - JUnit
 - JUnit perf
 - NUnit (.NET implementation of xUnit)
 - MbUnit
 - HTMLUnit
 - JWebUnit
