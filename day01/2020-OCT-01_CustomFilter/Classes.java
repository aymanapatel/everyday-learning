

/*
* Step I: Filter Interface
*/
public interface Filter {

  public void execute(String request);

}


/**
* Step II - Concrete classes
* 1. AuthenticationFilter
* 2. DebugFilter
*/
public class AuthenticationFilter implements Filter {

  public void execute (String request) {
    System.out.println("Authentication request: " + request);
  }
}

public class DebugFilter implements Filter {
  
  public void execute (String request) {
    System.out.println("DebugFilter request log: " + request);
  }
}


/*
* Step III: Target 
*/

public class Target {
  
  public void execute (String request) {
    System.out.println("Executing request: " + request);
  }
}




/*
* Step IV: Filter Chain
*/
import java.util.ArrayList;
import java.util.List;

public class FilterChain {

  private List<Filter> filtersList = new ArrayList<>();
  private Target target;
  
  public void addFilter (Filter filter) {
    filtersList.add(filter);
  }
  
  public void execute (String request) {
    for (Filter filter: filtersList) {
      filter.execute(request);
    }
    target.execute(request);
  }

}



/*
* Step V: Filter Manager
*/
public class FilterManager {

 private FilterChain fitlerChain;
 
 public FilterManager filterManager(Target target) {
  filterChain = new FilterChain();
  fitlerChain.setTarget(target);
 }
 
 public void setFilter(Filter filter) {
  filterChain.addFilter(filter);
 }
 
 public void filterRequest(String request) {
  filterChain.execute(request);
 }
  
}


/*
* Step VI: Client
*/
public class Client {

  private FilterManager filterManager;
  
  public void setFilterManager(FilterManager filterManager) {
    this.filterManager = filterManager;
  }
  
  public void sendRequest(String request) {
    filterManager.filterRequest(request);
  }
  
  
}


/*
* Step VII: Main Class
*/

public class Main {
  public static void main(String[] args) {
    
    FilterManager filterManager = new FilterManager(new Target());
    filterManager.setFilter(new AuthenticationFilter());
    filterManager.setFilter(new DebugFilter());
    
    Client client = new Client();
    client.setFitlerManager(filterManager);
    client.sendRequest("Testing filter"); 
  }

}



/*

Output:
Authenticating request: Testing filter
DebugFilter request log:  Testing filter
Executing request: HOME

*/







